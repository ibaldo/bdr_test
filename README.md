# README #

Este README contem instruções para instalação e execução das questões da prova enviada pela BDR como parte de processo seletivo para empresa ainda desconhecida por mim.

### Questões 1, 2 e 3 ###
Para fins de praticidade não haverá maiores instruções para as questões 1, 2 e 3. 

Para a questão 1, basta hospedar o arquivo em diretório acessível via web e executá-lo.

Para as questões 2 e 3, que são apenas de análise do código refatorado, os arquivos podem ser hospedados em qualquer lugar para avaliação.

### Questão número 4 ###

#### Descrição rápida da aplicação ####
Trata-se de uma API desenvolvida para listagem, detalhamento, inclusão, edição e remoção de uma lista de compromissos (Appointments) consumidas via REST

#### Configurações da aplicação ####

* Desenvolvida com CakePHP 3.6 baixada diretamente do site [https://cakephp.org/](https://cakephp.org/)
* Utlização do servidor web Apache/2.4.7 (Ubuntu)
* Banco de dados MySQL 
* Desenvolvida sob uso da versão 5.6 (Enquanto desenvolvo este README percebi que o requisito era desenvolver sob PHP 5.3 e que o Cake 3.6 necessita do PHP 5.6+. Caso isso seja um problema peço uma prazo extendido para que eu possa fazer o downgrade da aplicação)
* Requerida utlização de um programa cliente para consumo da API (sugere-se o uso da extensão [Advanced REST Client para Google Chrome](https://chrome.google.com/webstore/detail/advanced-rest-client/hgmloofddffdnphfgcellkdfbfbjeloo?hl=pt-BR))
* Observadas normas presentes na [PSR-2] (https://www.php-fig.org/psr/psr-2/)


### Instalação ###

#### Base de Dados ####
Importar o script disponível em https://bitbucket.org/ibaldo/bdr_test/src/master/bdr_n4/bdr.sql diretamente para o banco de dados MySQL.

Alterar o arquivo config/app.php com as configurações da conexão com seu banco de dados

```
 ... 

 'Datasources' => [
        'default' => [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Mysql',
            'persistent' => false,
            'host' => 'localhost', // INSIRA O HOST DA CONEXÃO AQUI
            /*
             * CakePHP will use the default DB port based on the driver selected
             * MySQL on MAMP uses port 8889, MAMP users will want to uncomment
             * the following line and set the port accordingly
             */
            //'port' => 'non_standard_port_number',
            'username' => 'MYUSER', // INSIRA O USUÁRIO DA CONEXÃO AQUI
            'password' => 'XXXPASSWORDXXX', // INSIRA A SENHA DA CONEXÃO AQUI
            'database' => 'bdr', // INSIRA O NOME DO BANCO DE DADOS AQUI
 ...

```


#### Arquivos da aplicação ####

Para instalar os arquivos, efetuar o clone da aplicação diretamente do Bitbucket

```
git clone https://ibaldo@bitbucket.org/ibaldo/bdr_test.git
```

Utilização das seguintes configurações de usuário e senha:

```
usuário: bdrtest2018 senha: bdrtest2018
```

Para que a API funcione conforme os exemplos que virão a seguir, o sistema deve funcionar com URL amigáveis. Para isso é necessário observar as configurações de reescrita de URL disponíveis no site do cake:

[https://book.cakephp.org/3.0/pt/installation.html#reescrita-de-url](https://book.cakephp.org/3.0/pt/installation.html#reescrita-de-url)



### Execução da API ###

Para fins didáticos será considerado que a API será consumida a partir do seguinte endereço: http://localhost/bdr/4/bdr_n4/

Serviços disponíveis:



#### Listagem ####
URL: http://localhost/bdr/4/bdr_n4/appointments/index.json



#### Detalhamento ####
URL: http://localhost/bdr/4/bdr_n4/appointments/view/1.json

**Atenção**: esta url serve para detalhamento do registro de appointment de id 1!



#### Inclusão ####
URL: http://localhost/bdr/4/bdr_n4/appointments/add.json



#### Alteração ####
URL: http://localhost/bdr/4/bdr_n4/appointments/edit/1.json

**Atenção**: esta url serve para alteração do registro de appointment de id 1!



#### Exclusão ####
URL: http://localhost/bdr/4/bdr_n4/appointments/delete/1.json

**Atenção**: esta url serve para exclusão do registro de appointment de id 1!



#### Exemplo de consumo da API via Advanced REST Client ####
Para fins didáticos o exemplo de uso da API será disponibilizado através de um vídeo. 

[https://youtu.be/tZyLawK6HS0](https://youtu.be/tZyLawK6HS0)

