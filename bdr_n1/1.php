<?php
/**
 * PrintNumber Handling 
 *
 * Show numbers from  1 to 100
 */
class printNumber {



    /**
     * Constructor...
     *
     */
	public function __construct() 
	{

	}

	 /**
     * verify if a number is multiple of 5.
     *
     * @param Integer $var
     * @return boolean true if number is multiple of 5.
     */
	private function isMultipleOf5($var) 
	{
		return  $var % 5 == 0;
	}

	 /**
     * verify if a number is multiple of 3.
     *
     * @param Integer $var
     * @return boolean true if number is multiple of 3.
     */	
	private function isMultipleOf3($var) 
	{
		return  $var % 3 == 0;
	}

	 /**
     * verify if a number is multiple of 3 and 5.
     *
     * @param Integer $var
     * @return boolean true if number is multiple of 3 and 5.
     */
	private function isMultipleOf3And5($var) 
	{
		return  $var % 5 == 0 && $var % 3 == 0;
	}

	 /**
     * Show the numbers...
     *
     */
	public function _print() 
	{
		$i=1;
		while ($i <= 100) {
			if ($this->isMultipleOf3And5($i)) {
				echo 'FizzBuzz' .'<br>';
				$i++;
				continue;	
			}	

			if ($this->isMultipleOf5($i)) {
				echo 'Fizz' .'<br>';				
			} elseif ($this->isMultipleOf3($i)) {
				echo 'Buzz' .'<br>';				
			} else {
				echo $i . '<br>';	
			}			
			$i++;
		}		
	}
}

$printNumber = new printNumber();
$printNumber->_print();






