<?php
	class MyUserClass
	{
		/**
		 * getUserList
		 *
		 * Returns a list of users
		 * 
		 */
		public function getUserList()
		{
			$dbconn = DB::getInstance();
			$results = $dbconn->query('select name from user');
			sort($results);
			return $results;
		}

	}

	class DB {

	    public static $instance;

	    private $host;
	    private $user;
	    private $password;
	    private $database;
	    
	    // connection with database var
	    public $connection;

	    
		/**
		 * __construct
		 *
		 * private constructor for singleton
		 * 
		 */
	    private function __construct() {
	    	$this->host = 'localhost';
	    	$this->user = 'user';
	    	$this->host = 'password';

			$this->connection = new mysqli($this->host, $this->user, $this->password, $this->database);
		
			// Error handling
			if(mysqli_connect_error()) {
				trigger_error("Failed to conencto to MySQL: " . mysql_connect_error(),
					 E_USER_ERROR);
			}    	

	    }

		/**
		 * getInstance
		 *  @static
		 *  get instance by singleton!!!
		 *  @return instance of DB
		 * 
		 */
	    public static function getInstance() {
	        
	        if (empty(self::$instance)) {
	            // creates an instance
	            self::$instance = new DB();
	        }
	        return self::$instance;
	    }

		/**
		 * query
		 *
		 * Execute a query
		 * @param String $sql a sql query
		 * @return query executed
		 */
	    public function query($sql) {
	    	if (!$this->connection) {
	    		die('error!')
	    	}
	    	return $this->connection->query($sql);
	    }
   
    }	