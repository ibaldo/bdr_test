<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         3.3.4
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use App\Controller\AppController;


/**
 * Appointments Handling Controller
 *
 * Controller used by web service via REST 
 */
class AppointmentsController extends AppController
{


    /**
     * Initialize configs of this Controller
     *
     */    
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }


    /**
     * Show a list of Appointments
     *
     */    
    public function index()
    {
        $appointments = $this->Appointments->find('all');
        $this->set([
            'appointments' => $appointments,
            '_serialize' => ['appointments']
        ]);
    }

    /**
     * Show an especific Appointment via rest
     *
     * @param Integer $id
     */    
    public function view($id)
    {
        $recipe = $this->Appointments->get($id);
        $this->set([
            'recipe' => $recipe,
            '_serialize' => ['recipe']
        ]);
    }

    /**
     * Add a new Appointment via rest
     *
     */    
    public function add()
    {

        $recipe = $this->Appointments->newEntity($this->request->getData());
        if ($this->Appointments->save($recipe)) {
            $message = 'Saved';
        } else {
            $message = 'Error';
        }
        $this->set([
            'message' => $message,
            'recipe' => $recipe,
            '_serialize' => ['message', 'recipe']
        ]);
    }

    /**
     * Edit an Appointment via rest
     *
     * @param Integer $id
     */    
    public function edit($id)
    {
        $recipe = $this->Appointments->get($id);
        if ($this->request->is(['post', 'put'])) {
            $recipe = $this->Appointments->patchEntity($recipe, $this->request->getData());
            if ($this->Appointments->save($recipe)) {
                $message = 'Saved';
            } else {
                $message = 'Error';
            }
        }
        $this->set([
            'message' => $message,
            '_serialize' => ['message']
        ]);
    }

    /**
     * Delete an Appointment via rest
     *
     * @param Integer $id
     */    
    public function delete($id)
    {
        $recipe = $this->Appointments->get($id);
        $message = 'Deleted';
        if (!$this->Appointments->delete($recipe)) {
            $message = 'Error';
        }
        $this->set([
            'message' => $message,
            '_serialize' => ['message']
        ]);
    }
}