<?php
$sessionLoggedin = isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true;
$cookieLoggedin  = isset($_COOKIE['Loggedin']) && $_COOKIE['Loggedin'] == true;

if ($sessionLoggedin || $cookieLoggedin) {
	header("Location: http://www.google.com");
	exit();
}